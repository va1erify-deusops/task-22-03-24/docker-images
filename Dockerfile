FROM alpine as builder
WORKDIR /app
RUN apk add --no-cache nodejs npm
COPY . .
RUN npm install --only=production

FROM alpine
WORKDIR /app
RUN apk add --no-cache nodejs
COPY --from=builder /app/src ./src
COPY --from=builder /app/node_modules ./node_modules
EXPOSE 3000
CMD ["node", "src/index.js"]